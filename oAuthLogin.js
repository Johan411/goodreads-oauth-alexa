var url = require('url');
var location = require('location-href');

const myCredentials = {
    key: 'rZPmFOvjHV6Mt12tn0lw',
    secret: 'bilqTsvBgwWtPHMHwWiRoPei9LyRhKSN8tEzXQjGKU'
};

// new_key = "rZPmFOvjHV6Mt12tn0lw";
// new_secret = "bilqTsvBgwWtPHMHwWiRoPei9LyRhKSN8tEzXQjGKU";

var try_oauth = function(context) {
    // AFAIK, goodreads doesn't support OAuth2.0
    var OAuth = require('oauth').OAuth;

    var OAUTH = new OAuth("https://www.goodreads.com/oauth/request_token", "https://www.goodreads.com/oauth/access_token", myCredentials.key, myCredentials.secret, '1.0', "http://localhost:3000", 'HMAC-SHA1');

    OAUTH.getOAuthRequestToken(function(err, oAuthToken, oAuthTokenSecret, results) {
        if (err) {
            console.log("Oops! Failed to get request token");
        } else {
            var authorize_url = "https://www.goodreads.com/oauth/authorize?oauth_token=" + oAuthToken;
            // context.emit(":ask", "Please visit this url");
            console.log(authorize_url);
            console.log("Redirecting user to the same ...");

            location.set(authorize_url);

            setTimeout(function() {
                console.log("Waited for 60s. Trying to get access token ...");
                // console.log("userid: " + context.event.session.user.userId);
                console.log("OAuth token: " + oAuthToken);
                console.log("OAuth token secret: " + oAuthTokenSecret);
                OAUTH.getOAuthAccessToken(oAuthToken, oAuthTokenSecret, function(err, access_token, access_token_secret) {
                    console.log("Access token: " + access_token);
                    console.log("Access token secret: " + access_token_secret);
                    // context.emit("BibliophileHelp");
                });
            }, 5000);
        }

    });
}

module.exports = {
    try_oauth: try_oauth
};

// try_oauth(this);