
var AWS = require("aws-sdk");

// use this only for testing & ensure that dynamodb is running locally on port 8000


AWS.config.update({
    region: "us-west-2",
    endpoint: "http://localhost:8000"
});

console.log(AWS.config);

var ddb_crud = require("./dynamodb_crud");

// Use this for creating tables

var params = {
    TableName: "Oauth_table",
    KeySchema: [{
        AttributeName: "user_id",
        KeyType: "HASH"
    }],
    AttributeDefinitions: [{
        AttributeName: "user_id",
        AttributeType: "S"
    }],
    ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
    }
};

function hello() {
    var docClient = new AWS.DynamoDB.DocumentClient()

    var table = "Authordetails_table";

    var params = {
        TableName: table,
        Key: {
            "authorname": "Robert Frost"
        }
    };

    docClient.get(params, function(err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
        }
    });
}

// ddb_crud.ddb_create_table(params);

// hello();

// ddb_crud.ddb_read_entry("test_table1", {authorid: "25"});

function foobar(context, data) {
    if (data == null)
        console.log("Not present in db");
    else {
        console.log(data);
	console.log(data.Items[0].details);
    }
};

// ddb_crud.ddb_add_entry("Oauth_table", {user_id: "123", details: {"oauth": "xyz"}}, this, foobar);
ddb_crud.ddb_query_entry("Oauth_table", {user_id: "123"}, this, foobar);
// ddb_crud.ddb_read_entry("Authordetails_table", {
//     "authorname": "Robert"
// }, this, foobar);
