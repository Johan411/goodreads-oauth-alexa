'use strict';

exports.handler = (event, context, callback) => {
    console.log('Loading oauth-goodreads lambda function');
    console.log("Inside handler");
    console.log("Events : " + JSON.stringify(event, null, 4));
    console.log("Context : " + JSON.stringify(context, null, 4));

    const myCredentials = {
        key: 'rZPmFOvjHV6Mt12tn0lw',
        secret: 'bilqTsvBgwWtPHMHwWiRoPei9LyRhKSN8tEzXQjGKU'
    };

    var ddb = require("./dynamodb_crud")

    // var oauthlogin = require("./oAuthLogin");

    // AFAIK, goodreads doesn't support OAuth2.0
    var OAuth = require('oauth').OAuth;

    var OAUTH = new OAuth("https://www.goodreads.com/oauth/request_token", "https://www.goodreads.com/oauth/access_token", myCredentials.key, myCredentials.secret, '1.0', "http://localhost:8000", 'HMAC-SHA1');

    console.log("Before switch");

    switch (event.path) {
        case '/myGoodreadsCallback/getRequestToken':
            OAUTH.getOAuthRequestToken(function(err, oAuthToken, oAuthTokenSecret, results) {
                if (err) {
                    console.log("Oops! Failed to get request token");
                } else {
                    console.log("Event : " + JSON.stringify(event, null, 4));
                    var authorize_url = "https://www.goodreads.com/oauth/authorize?oauth_token=" + oAuthToken;
                    // context.emit(":ask", "Please visit this url");
                    console.log(authorize_url);
                    console.log("Redirecting user to the same ...");

                    console.log(oAuthToken);
                    console.log(oAuthTokenSecret);

                    console.log("Event : " + JSON.stringify(event, null, 4));
                    console.log("Context : " + JSON.stringify(context, null, 4));

                    // Save the tokens to oauth_table

                    var item = {
                        'user_id': "bibliophile",
                        'tokens': {
                            'state': event.state,
                            'response_type': event.response_type,
                            'scope': event.scope,
                            'redirect_uri': event.redirect_uri,
                            'client_id': event.client_id,
                            'oauth_token': oAuthToken,
                            'oauth_secret': oAuthTokenSecret
                        }
                    };

                    // Dirty hack so that we can do an HTTP redirection to goodreads

                    // Returns 302

                    var redirect = function(return_context, return_data) {
                        console.log("Inside redirect");
                        var ret = new Error("HandlerDemo.ResponseFound Redirection: Resource found elsewhere");
                        ret.name = authorize_url;
                        console.log(ret);

                        context.done(ret, null);

                    }

                    ddb.ddb_add_entry('Oauth_table', item, context, redirect)

                }
            });
            break;
        case '/myGoodreadsCallback/getAccessToken':
            console.log("Inside access");
            console.log("Event: " + JSON.stringify(event, null, 4));

            var success_url = "https://alexa.amazon.co.jp/spa/skill/account-linking-status.html?success=true";
            var failure_url = "https://alexa.amazon.co.jp/spa/skill/account-linking-status.html?success=false";
            var redirect_url;

            if (event.queryParams.authorize == "1") {
                console.log("Woohoo!");
                redirect_url = success_url;
            } else {
                console.log("failed ...");
                redirect_url = failure_url;
            }

            var item = {
                'user_id': "bibliophile"
            };

            ddb.ddb_query_entry('Oauth_table', item, context, function(return_context, return_data) {

                console.log("Inside last callback");

                var oAuthToken = return_data.Items[0].tokens.oauth_token;
                var oAuthTokenSecret = return_data.Items[0].tokens.oauth_secret;
                var redirect_uri_orig = return_data.Items[0].tokens.redirect_uri;
                var scope_orig = return_data.Items[0].tokens.scope;
                var state_orig = return_data.Items[0].tokens.state;
                var client_id_orig = return_data.Items[0].tokens.client_id;
                var response_type_orig  = return_data.Items[0].tokens.response_type;

                OAUTH.getOAuthAccessToken(oAuthToken, oAuthTokenSecret, function(err, access_token, access_token_secret) {
                    console.log("Access token: " + access_token);
                    console.log("Access token secret: " + access_token_secret);

                    var item = {
                        'user_id': "bibliophile",
                        'tokens': {
                            'state': return_data.Items[0].tokens.state,
                            'client_id': return_data.Items[0].tokens.client_id,
                            'response_type': return_data.Items[0].tokens.response_type,
                            'scope': return_data.Items[0].tokens.scope,
                            'redirect_uri': return_data.Items[0].tokens.redirect_uri,
                            'oauth_token': oAuthToken,
                            'oauth_secret': oAuthTokenSecret,
                            'access_token': access_token,
                            'access_token_secret': access_token_secret
                        }
                    };

                    var redirect = function(return_context, return_data) {
                        console.log("Inside redirect");
                        var ret = new Error("HandlerDemo.ResponseFound Redirection: Resource found elsewhere. Redirecting back to Alexa");
                        ret.name = redirect_uri_orig + "#access_token=" + access_token + "," + access_token_secret +
                            "&state=" + state_orig +
                            "&client_id=" + client_id_orig +
                            "&response_type=Bearer";
                        console.log(ret);

                        context.done(ret, null);

                    }

                    ddb.ddb_add_entry('Oauth_access_table', item, context, redirect);

                });
            });

            break;
    }
};