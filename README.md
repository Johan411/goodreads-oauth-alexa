Custom OAuth1.0 implementation for Goodreads
============================================

The OAuth support for Goodreads has some problems and it's tough to incorporate this
with Amazon Alexa's custom app since Alexa supports OAuth2.0 and Goodreads doesn't
comply with the RFC for Oauth1.0 (https://tools.ietf.org/html/rfc5849)

This will act as a middleware for enabling account linking with Alexa.